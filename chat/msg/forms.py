from django import forms
from django.contrib.auth.models import User


class UserRegistrationForm(forms.ModelForm):
    password = forms.CharField(label="Haslo", widget=forms.PasswordInput)
    repeat_password = forms.CharField(label="Powtorz haslo", widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('username',)

    def clean_repeat_password(self):
        if self.cleaned_data['password'] != self.cleaned_data['repeat_password']:
            raise forms.ValidationError("Passwords are not matching")
        return self.cleaned_data['repeat_password']
