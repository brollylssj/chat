from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models import Count
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.http import require_http_methods
from django.views.generic import TemplateView
from django.contrib import messages
from chat_app import settings
from msg.forms import UserRegistrationForm
from .models import Chat


class LoginView(TemplateView):
    def get(self, request):
        return render(request, "msg/login.html")

    def post(self, request):
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/home/')
            else:
                return HttpResponse("Account is not active at the moment.")
        else:
            return render(request, "msg/login.html")


class RegisterAccountView(TemplateView):
    form_class = UserRegistrationForm()
    template_name = 'msg/register.html'
    template_done = 'msg/register_done.html'

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name, {'user_form': self.form_class})

    def post(self, request):
        user_form = UserRegistrationForm(request.POST)
        if user_form.is_valid():
            new_user = user_form.save(commit=False)
            new_user.set_password(user_form.cleaned_data['password'])
            new_user.save()
            return render(request, self.template_done, {'user_form': user_form})
        return render(request, self.template_name, {'user_form': user_form})


class LogoutView(TemplateView):
    def get(self, request, *args, **kwargs):
        logout(request)
        return HttpResponseRedirect('/login/')


class ChatView(TemplateView):
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        chat = Chat.objects.all()
        return render(request, "msg/chat.html", {'home': 'active', 'chat': chat})


class SendMessageView(TemplateView):
    def post(self, request):
        msg = request.POST.get('msgbox', None)
        chat = Chat(user=request.user, message=msg)
        if msg != '':
            chat.save()
        return JsonResponse({'msg': msg, 'user': chat.user.username})


class MessageView(TemplateView):
    def get(self, request, *args, **kwargs):
        chat = Chat.objects.all()
        return render(request, 'msg/messages.html', {'chat': chat})


class CheckUsernameAvailability(TemplateView):
    def post(self, request):
        user = User.objects.filter(username=request.POST.get('username'))
        if user:
            return JsonResponse({'exist': False})
        return JsonResponse({'exist': True})