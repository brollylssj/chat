from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^login/$', views.LoginView.as_view(), name='login'),
    url(r'^logout/$', views.LogoutView.as_view(), name='logout'),
    url(r'^register/$', views.RegisterAccountView.as_view(), name='register'),
    url(r'^home/$', views.ChatView.as_view(), name='home'),
    url(r'^post/$', views.SendMessageView.as_view(), name='post'),
    url(r'^messages/$', views.MessageView.as_view(), name='messages'),
    url(r'^check_username_availability/$', views.CheckUsernameAvailability.as_view(), name='check_username_availability')
]

#(?P<username>[\w-]+)/